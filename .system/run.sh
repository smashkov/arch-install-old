#!/bin/bash
#
# Usage:
# run.sh <script> <first message> <last message> <root?>

SCRIPT=$1
FIRST_MSG=$2
LAST_MSG=$3
ROOT=$4

if [[ "$ROOT" == "root" ]] && [[ `id -u` > 0 ]]; then
    echo "This script must be run by root only!"
    exit
fi

if [[ "$ROOT" != "root" ]] && [[ `id -u` == 0 ]]; then
    echo "This script must not be run by root!"
    exit
fi

FINAL=""
if [ -n "$LAST_MSG" ]; then
    FINAL="$LAST_MSG"
fi

if [ -n "$FIRST_MSG" ]; then
    echo "$FIRST_MSG"
fi
$SCRIPT && echo "$FINAL!"
