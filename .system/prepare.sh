#!/bin/bash
#
# Usage:
# prepare.sh <root?>

ROOT=$1

CURRENT_DIR=`pwd`
BASENAME=`basename $CURRENT_DIR`
TMP_DIR=/tmp/$BASENAME
RUNNER=`realpath ../../.system/run.sh`

rm -rf $TMP_DIR
cp -r common $TMP_DIR
cp -r ../../.common/* $TMP_DIR

if [ -d lenovo-x1-yoga-g8 ]; then
    pushd . > /dev/null
    cd lenovo-x1-yoga-g8
    $RUNNER "./patch.sh $TMP_DIR" "Patching scripts for Lenovo X1 Yoga G8" "Done" "$ROOT"
    popd > /dev/null
fi

pushd . > /dev/null
cd $TMP_DIR
./actions.sh $RUNNER "$ROOT"
popd > /dev/null

rm -rf $TMP_DIR
