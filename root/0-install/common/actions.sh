#!/bin/bash
#
# Usage:
# actions.sh <runner> <root?>

RUNNER=$1
ROOT=$2

$RUNNER ./timezone.sh "Setting timezone" "Done" "$ROOT"
$RUNNER ./locales.sh "Setting up locales" "Done" "$ROOT"
$RUNNER ./git.sh "Setting up git" "Done" "$ROOT"
$RUNNER ./reflector.sh "Running reflector scripts" "Done" "$ROOT"

./actions-common.sh $RUNNER "$ROOT"

$RUNNER ./mkinitcpio.sh "Setting up mkinitcpio initramfs" "Done" "$ROOT"
$RUNNER ./hostname.sh "Setting up hostname" "Done" "$ROOT"
$RUNNER ./bootmanager.sh "Setting up boot manager" "Done" "$ROOT"
$RUNNER ./root-password.sh "Setting up root password" "Done" "$ROOT"
