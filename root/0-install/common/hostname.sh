#!/bin/bash

HOSTNAME=`cat hostname`

echo $HOSTNAME > /etc/hostname && echo "hostname set to $HOSTNAME"
cat << EOF > /etc/hosts && echo "/etc/hosts patched"
127.0.0.1	localhost
::1		localhost
127.0.1.1	$HOSTNAME.localdomain	$HOSTNAME
EOF
