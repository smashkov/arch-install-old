#!/bin/bash

MODULES=`cat modules`

awk -i inplace "{if (match(\$1, /^MODULES=/)) {print \"MODULES=($MODULES)\"} else {print \$0}}" /etc/mkinitcpio.conf && echo "mkinitcpio modules patched with ($MODULES)"

HOOKS=`cat hooks`

awk -i inplace "{if (match(\$1, /^HOOKS=/)) {print \"HOOKS=($HOOKS)\"} else {print \$0}}" /etc/mkinitcpio.conf && echo "mkinitcpio hooks patched with ($HOOKS)"

mkinitcpio -P
