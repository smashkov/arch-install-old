#!/bin/bash

bootctl install

echo "Enter root device (for instance /dev/arch/root):"
read ROOT
echo "Enter swap device (for instance /dev/arch/swap):"
read SWAP
cp --force boot/loader/entries/arch.conf /boot/loader/entries/arch.conf && echo "Arch configuration is copied"
echo "options root=$ROOT rw quiet resume=$SWAP lsm=lockdown,yama,apparmor ipv6.disable=1 nowatchdog" >> /boot/loader/entries/arch.conf && echo "Kernel options set up with root=$ROOT and resume=$SWAP"

awk -i inplace "BEGIN{FS=\" \"} {if (match(\$1, \"default\")) {print \$1, \"arch.conf\"} else {print \$0}}" /boot/loader/loader.conf && echo "Arch configuration set as default"
