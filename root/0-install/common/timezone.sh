#!/bin/bash

TIMEZONE=`cat timezone`

ln -sf /usr/share/zoneinfo/$TIMEZONE /etc/localtime && echo "Localtime set to $TIMEZONE"

hwclock --systohc && echo "Hardware clock synced"
timedatectl set-ntp true
