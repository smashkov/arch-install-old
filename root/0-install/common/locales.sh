#!/bin/bash

while read l; do
	awk -i inplace "BEGIN{FS=\" \"; OFS=\" \"} {if (match(\$1, /^#$l/)) {print substr(\$1, 2), \$2} else {print \$0}}" /etc/locale.gen
done < locales && echo "/etc/locale.gen patched"

locale-gen

LANG=`cat lang`
LC_TIME=`cat lc_time`

cat << EOF > /etc/locale.conf && echo "/etc/locale.conf generated with LANG=$LANG"
LANG=$LANG
LC_TIME=$LC_TIME
EOF

KEYMAP=`cat keymap`

echo "KEYMAP=$KEYMAP" > /etc/vconsole.conf && echo "/etc/vconsole.conf generated with KEYMAP=$KEYMAP"
