#!/bin/bash

EMAIL=`cat git-email`
USERNAME=`cat git-username`

pacman -S --needed git

git config --global user.name "$USERNAME" && echo "Username set to \"$USERNAME\""
git config --global user.email i"$EMAIL" && echo "E-mail set to \"$EMAIL\""
