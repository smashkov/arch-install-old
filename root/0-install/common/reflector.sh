#!/bin/bash

pacman -S --needed reflector

reflector --sort rate --country Russia --latest 5 --protocol https --save /etc/pacman.d/mirrorlist

awk -i inplace "BEGIN{FS=\" \"; OFS=\" \"} {if (match(\$1, \"--sort\")) {print \$1, \"rate\"} else {print \$0}}" /etc/xdg/reflector/reflector.conf
awk -i inplace "BEGIN{FS=\" \"; OFS=\" \"} {if (match(\$2, \"--country\")) {print \$2, \"Russia\"} else {print \$0}}" /etc/xdg/reflector/reflector.conf
