#!/bin/bash
#
# Usage:
# patch.sh <target directory>

TARGET=$1

cat packages >> $TARGET/packages && echo "Lenovo X1 Yoga G8 specific packages added"
patch -p1 $TARGET/boot/loader/entries/arch.conf < arch.conf.diff && echo "Boot config patched for Lenovo X1 Yoga G8"
cp modules $TARGET/modules && echo "Modules replaced"
cp hostname $TARGET/hostname && echo "Hostname replaced"
