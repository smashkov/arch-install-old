#!/bin/bash
#
# Usage:
# patch.sh <target directory>

TARGET=$1

cat packages >> $TARGET/packages && echo "Lenovo X1 Yoga G8 specific packages added"
patch -p1 /etc/pam.d/system-login < system-login.diff
patch -p1 /etc/pam.d/sudo < sudo.diff
patch -p1 /etc/pam.d/polkit-1 < polkit-1.diff
