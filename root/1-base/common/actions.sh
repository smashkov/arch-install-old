#!/bin/bash
#
# Usage:
# actions.sh <runner> <root?>

RUNNER=$1
ROOT=$2

$RUNNER ./pacman-color.sh "Making pacman colorful" "Done" "$ROOT"

./actions-common.sh $RUNNER "$ROOT"

$RUNNER ./sudo.sh "Configuring sudoers" "Done" "$ROOT"
$RUNNER ./redshift.sh "Setting up redshift" "Done" "$ROOT"
$RUNNER ./user.sh "Creating user" "Done" "$ROOT"
$RUNNER ./leds.sh "Adding access to leds" "Done" "$ROOT"
$RUNNER ./ufw.sh "Setting default ufw rules" "Done" "$ROOT"
