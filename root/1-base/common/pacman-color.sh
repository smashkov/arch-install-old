#!/bin/bash

awk -i inplace "{if (match(\$1, \"#Color\")) {print substr(\$1, 2), \$2} else {print \$0}}" /etc/pacman.conf
