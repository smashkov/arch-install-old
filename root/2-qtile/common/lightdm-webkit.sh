#!/bin/bash

awk -i inplace "BEGIN{FS=\"=\"; OFS=\"=\"} {if (match(\$1, /^#greeter-session/)) {print substr(\$1, 2), \"lightdm-webkit2-greeter\"} else {print \$0}}" /etc/lightdm/lightdm.conf
