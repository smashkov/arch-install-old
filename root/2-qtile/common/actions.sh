#!/bin/bash
#
# Usage:
# actions.sh <runner> <root?>

RUNNER=$1
ROOT=$2

./actions-common.sh $RUNNER "$ROOT"

$RUNNER ./lightdm-webkit.sh "Patching lightdm.conf to use the correct greeter" "Done" "$ROOT"

echo "Enter username to set up the accountservice for:"
read USER;

$RUNNER "./accountsservice.sh $USER" "Setting accountservice info for user $USER" "Done" "$ROOT"
