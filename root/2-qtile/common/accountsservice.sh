#!/bin/bash
#
# Usage:
# accountsservice.sh <username>

USERNAME=$1
FILE=/var/lib/AccountsService/users/$USERNAME
ICON=/var/lib/AccountsService/icons/$USERNAME.png

cp avatar.png $ICON
echo "[User]" > $FILE
echo "XSession=qtile" >> $FILE
echo "Icon=$ICON" >> $FILE
echo "SystemAccount=false" >> $FILE
