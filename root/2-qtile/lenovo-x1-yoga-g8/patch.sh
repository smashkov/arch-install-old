#!/bin/bash

if [[ $@ < 2 ]]; then
	echo "Usage: patch.sh <target directory>"
	exit
fi

pacman -S sudo --needed

TARGET=$1
cp -rf etc $TARGET/etc && echo "Synaptics touchpad settings added"
