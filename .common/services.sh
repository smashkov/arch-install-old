#!/bin/bash
#
# Usage:
# services.sh <root?>

ROOT=$1

if [ -f "services" ]; then
    if [[ "$ROOT" == 'root' ]]; then
        while read SERVICE; do
            systemctl enable $SERVICE
        done < services
    else
        while read SERVICE; do
            systemctl enable $SERVICE --user
        done < services
    fi
fi
