#!/bin/bash
#
# Usage:
# packages.sh <root?>

ROOT=$1

if [ -f "packages" ]; then
    if [[ "$ROOT" == 'root' ]]; then
        pacman -S --needed - < packages
    else
        paru -S --needed - < packages
    fi
fi
