#!/bin/bash
#
# Usage:
# actions.sh <runner> <root?>

RUNNER=$1
ROOT=$2

$RUNNER ./etc.sh "Updating /etc configs" "Done" "$ROOT"
$RUNNER ./packages.sh "Installing packages" "Done" "$ROOT"
$RUNNER ./services.sh "Enabling systemd services" "Done" "$ROOT"
