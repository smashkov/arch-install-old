#!/bin/bash
#
# Usage:
# patch.sh <target directory>

TARGET=$1

patch -p1 $TARGET/dotfiles.sh < dotfiles.diff && echo "Lenovo X1 Yoga G8 dotfiles fix applied"
