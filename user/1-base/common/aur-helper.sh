#!/bin/bash

TMP_DIR="helper"
AUR_HELPER_REPO=`cat aur-helper-repo`

git clone $AUR_HELPER_REPO $TMP_DIR

pushd . > /dev/null
cd $TMP_DIR
makepkg -si
popd > /dev/null

rm -rf $TMP_DIR
