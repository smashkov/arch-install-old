#!/bin/bash

DOTFILES_DIR=$HOME/.dotfiles.git
DOTFILES_REPO=`cat dotfiles-repo`

cd ~
rm -rf $DOTFILES
git clone --bare $DOTFILES_REPO $DOTFILES_DIR
git --git-dir=$DOTFILES_DIR --work-tree=$HOME config status.showUntrackedFiles no
git --git-dir=$DOTFILES_DIR --work-tree=$HOME pull --force
git --git-dir=$DOTFILES_DIR --work-tree=$HOME push --set-upstream origin main
