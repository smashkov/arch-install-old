#!/bin/bash
#
# Usage:
# actions.sh <runner> <root?>

RUNNER=$1
ROOT=$2

$RUNNER ./aur-helper.sh "Installing AUR helper" "Done" "$ROOT"
$RUNNER ./dotfiles.sh "Installing dotfiles" "Done" "$ROOT"
$RUNNER ./git.sh "Configuring git user" "Done" "$ROOT"

./actions-common.sh $RUNNER "$ROOT"
