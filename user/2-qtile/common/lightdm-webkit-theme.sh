#!/bin/bash

THEME=`cat lightdm-webkit-theme`

sudo awk -i inplace "BEGIN{FS=\"=\"; OFS=\"=\"} {if (match(\$1, /^webkit_theme/)) {print \$1, \" $THEME\"} else {print \$0}}" /etc/lightdm/lightdm-webkit2-greeter.conf
