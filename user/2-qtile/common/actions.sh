#!/bin/bash
#
# Usage:
# actions.sh <runner> <root?>

RUNNER=$1
ROOT=$2

THEME=`cat lightdm-webkit-theme`

./actions-common.sh $RUNNER "$ROOT"

$RUNNER ./lightdm-webkit-theme.sh "Setting up lightdm theme to $THEME" "Done" "$ROOT"
