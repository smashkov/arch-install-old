#!/bin/bash -x
#
# Usage:
# actions.sh <runner> <root>

RUNNER=$1
ROOT=$2

./actions-common.sh $RUNNER "$ROOT"
